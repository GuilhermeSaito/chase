/* chase - chase a symbolic link

   Copyright (C) 1998 Antti-Juhani Kaijanaho

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA

   The author can be reached via mail at (ISO 8859-1 charset for the city)
      Antti-Juhani Kaijanaho
      Helvintie 2 e as 9
      FIN-40500 JYV�SKYL�
      FINLAND
      EUROPE
   and via electronic mail from
      gaia@iki.fi
   If you have a choice, use the email address; it is more likely to
   stay current.

*/

/* $Id: fname.c,v 1.2 1998/12/09 18:20:13 ajk Exp $ */

#include <assert.h>
#include <string.h>
#include <unistd.h>

#include "gcollect.h"
#include "fname.h"

/* from GNU libc 2.0 manual, slighty modified: now the return value is
   guaranteed to contain a slash at the end.*/
char *
gnu_getcwd (void)
{
  int size = 100;
  char *buffer = (char *) gc_xmalloc (size);
  size_t blen;
 
  while (1)
    {
      char *value = getcwd (buffer, size);
      if (value != 0)
	break;
      size += 20;
      buffer = (char *) gc_xmalloc (size);
    }
  blen = strlen (buffer);
  if (buffer [blen - 1] != '/')
    {
      if (size == blen + 1)
	buffer = gc_xrealloc (buffer, blen);
      buffer [blen] = '/';
      buffer [blen+1] = '\0';
    }
  return buffer;
}

/* Return a fresh string containing the parent directory part of the
   file name.  */
char *
fname_dir (const char *fname)
{
  char * rv;
  char * slash;

  assert (fname != 0);
  assert (*fname != '\0');

  rv = gc_xstrdup (fname);
  slash = strrchr (rv, '/');
  if (slash != 0)
      *slash = '\0';
  else
    rv = ".";
  /* An empty return value should really be "/".  */
  if (*rv == 0)
    {
      rv [0] = '/';
      rv [1] = '\0';
    }
  return rv;
}

/* Return a fresh string containing all but the parent directory part
   of the file name.  */
char *
fname_chopdir (const char *fname)
{
  char * rv;
  char * slash;

  assert (fname != 0);

  rv = gc_xstrdup (fname);
  slash = strrchr (rv, '/');
  if (slash != 0)
    rv = slash + 1;
  /* An empty return value should really be ".".  */
  if (*rv == 0)
    {
      rv [0] = '.';
      rv [1] = '\0';
    }
  return rv;
}

/* Return a fresh string containing an absolute path to fname.  fname
   is assumed NOT to be a symlink, so optimizations ("./" -> "") are
   made.  Tilde expansion is not made.  */
char *
fname_absolutify (const char * fname)
{
  char * wd;  /* Current work directory.  */
  char * rv;
  char * ptr;
  char * prev_ptr;

  wd = gnu_getcwd ();

  /* The return value cannot grow longer due to simplifications, so
     it's safe to allocate a tight buffer.  */
  if (fname [0] == '/')
    rv = gc_xstrdup (fname);
  else
    {
      rv = gc_xmalloc (strlen (wd) + strlen (fname) + 1);
      strcpy (rv, wd);
      strcat (rv, fname);
    }

  /* Now rv contains an absolute path name.  We must simplify it:
     remove all "/./"'s first. */
  ptr = rv; 
  while (ptr != 0)
    {
      ptr = strstr (ptr, "/./");
      if (ptr != 0)
	memmove(ptr, ptr + 2, strlen (ptr + 2) + 1);
    }

  /* Then, for every "/../" encountered, remove it and its preceding
     directory name, if any.  */
  ptr = rv;
  while (ptr != 0 && *ptr != '\0')
    {
      prev_ptr = ptr;
      ptr = strchr (ptr+1, '/');
      if (ptr != 0 && ptr [1] == '.' && ptr [2] == '.' && ptr [3] == '/')
	{
	  memmove (prev_ptr, ptr + 3, strlen (ptr + 3) + 1);
	  ptr = prev_ptr;
	}
    }

  return rv;
}
