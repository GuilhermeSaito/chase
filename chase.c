/* chase - chase a symbolic link

   Copyright (C) 1998, 1999, 2000 Antti-Juhani Kaijanaho

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA

   The author can be reached via mail at (ISO 8859-1 charset for the city)
      Antti-Juhani Kaijanaho
      Helvintie 2 e as 9
      FIN-40500 JYV�SKYL�
      FINLAND
      EUROPE
   and via electronic mail from
      gaia@iki.fi
   If you have a choice, use the email address; it is more likely to
   stay current.

*/

/* The algorithm is very simple: we check, whether the current file is
   a symlink, and if it is, we loop with the destination as the
   current file.  */

#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "config.h"
#include "gcollect.h"
#include "fname.h"

#define _(String) (String)
#define N_(String) (String)
#define textdomain(Domain)
#define bindtextdomain(Package, Directory)

/* This structure is used to form a list of file names for loop
   detection. */
struct filelist_t {
  char * fn;
  struct filelist_t * rest;
};

/* Globals */
char * program_name;  /* program_name points to argv[0]. */
int    verbose = 0;   /* Are we being verbose? */
int    record_chain_p = 1; /* Should we record the chain of symlinks? */
unsigned long int warn_count = 30; /* Warn after this many iterations. */
unsigned long int giveup_count = 500; /* Give up after this many iterations. */

/* The following macros are used to control what gets displayed and
   what doesn't. */
#if (NDEBUG == 1)
#  define CHAT(str,fn) do { ; } while (0)
#else /* NDEBUG != 1 */
#  define CHAT(str,fn) fprintf (stderr, _ ("Note (%s): %s\n"), fn, str)
#endif /* NDEBUG != 1 */

#define VERBOSE(str,fn) do { if (verbose) printf ("%s%s\n", str, fn); \
                           } while (0);

/* Print an error message concerning file fn in directory dir (must
   end in a slash), based on errno. */
#define PRERR(dir,fn) fprintf (stderr, "%s: %s%s: %s\n", program_name, \
			       dir, fn, strerror (errno))

/* Print an error message concerning file fn in directory dir (must
   end in a slash). */
#define ERRMSG(msg,f,dir) fprintf (stderr, "%s: %s%s: %s\n", program_name, \
                               dir, f, msg)

#define IMPLIES(p,q) ( (! (p)) || (q))

/* Change work directory to wd, with perroring on error.  Return from
   the parent function with return value err, if error happened.  The
   do...while(0) is a trick to allow xchdir be treated like a function
   call.  */
#define xchdir(wd,err) \
  do { \
    if (chdir (wd) == -1) \
      { \
        PRERR("",wd); \
        return (err); \
      } \
  } while (0)

/* Return a freshly allocated string whose content was read from f, or
   0 on error or end of file.  The return value is a block of text
   read from the file, whose end is signalled in both the file and the
   return value by a null character.  fname is used with status
   reporting. */
static char *
fread_until_null(FILE * f, const char * fname)
{
  char * rv;
  size_t i = 0;
  int c;
  size_t rvlen = 16;

  assert (f != 0);
  assert (fname != 0);

  rv = gc_xmalloc(rvlen);

  CHAT (_ ("reading..."), fname);
  while ( (c = getc(f)) != EOF)
    {
      if (i >= rvlen)
        rv = gc_xrealloc(rv, rvlen *= 2);
      *(rv + i++) = (unsigned char) c;
      if (c == 0)
        break;
    }
  if (c == EOF)
    {
      if (ferror(f))
        perror(0);
      return 0;
    }

  return rv;
}

/* Return a freshly allocated string whose content was read from f, or
   0 on error or end of file.  The return value is a complete text
   line read from the file, without the trailing newline.  fname is
   used with status reporting. */
static char *
freadline (FILE * f, const char * fname)
{
  char * rv;
  char * rvptr;  /* Where are we now writing? */
  size_t rvlen = 0;
  const size_t rvlen_inc = 80;

  assert (f != 0);
  assert (fname != 0);

  rvlen = rvlen_inc;
  rv = rvptr = gc_xmalloc (rvlen);

  *rvptr = '\0';

  do
    {
      char * result;

      assert (*rvptr == '\0');

      CHAT (_ ("reading..."), fname);
      result = fgets (rvptr, rvlen - (rvptr - rv), f);
      if (result == 0)
	return 0;

      /* We might write more... */
      rvptr = rvptr + strlen (rvptr);

      assert (rvptr >= rv);
      if (rvptr == rv || rvptr [-1] != '\n')
	{
	  ptrdiff_t rvptr_rv = rvptr - rv;

	  CHAT (_ ("line is longer than buffer, resizing..."), fname);
	  rvlen += rvlen_inc;
	  rv = gc_xrealloc (rv, rvlen);
	  rvptr = rv + rvptr_rv;
	}
    }
  while (rvptr == rv || rvptr [-1] != '\n');

  assert (rvptr > rv);
  assert (*rvptr == '\0');
  if (rvptr [-1] == '\n')
    rvptr [-1] = '\0';

  return rv;
}

/* Return value is a freshly allocated copy of the name of the file
   that is not itself a symlink but that is reachable via symlinks. */
static char *
chase_symlink (char * fname)
{
  char * buf = 0;
  char * wd;   /* the original work directory */
  size_t blen = 0;
  const size_t blen_inc = 20;  
  int result = 0;
  unsigned long int iter_count = 0;
  struct filelist_t * flist = 0;

  blen = blen_inc;
  buf = gc_xmalloc (blen);

  wd = fname_absolutify (gnu_getcwd ());

  /* We want to be in the directory that contains the file fname;
     therefore, we can only use its base name from now on. */
  xchdir (fname_dir (fname), 0);
  fname = fname_chopdir (fname);
  
  do
    {
      struct filelist_t * it;
      char * dir;
      char * fnamewithdir;
      
      assert (buf != 0);
      assert (blen != 0);
      assert (result != -1);
      
      dir = gnu_getcwd ();
      fnamewithdir = gc_xmalloc (strlen (dir) + strlen (fname) + 1);
      strcat (strcpy (fnamewithdir, dir), fname);

      /* Detect loops.  This is a O(n^2) algorithm (when combined with
         the enclosing loop), but it does not matter, as n is usually
         very small. */
      for (it = flist; it != 0; it = it->rest)
        {
          CHAT (_ ("it->fn"), it->fn);
          CHAT (_ ("fnamewithdir"), fnamewithdir);
          if (strcmp(it->fn, fnamewithdir) == 0)
            {
              ERRMSG (_ ("symlink loop detected, giving up..."), fname, dir);
              return 0;
            }
        }

      if (record_chain_p) 
        {
          struct filelist_t * new;

          new = gc_xmalloc_container (sizeof (struct filelist_t));

          new->fn = fnamewithdir;
          new->rest = flist;
          flist = new;
        }

      ++iter_count;

      if (giveup_count != 0 && iter_count >= giveup_count)
        {
          ERRMSG (_ ("too many symlink hops, giving up..."), fname, dir);
          return 0;
        }

      if (warn_count != 0 &&
          iter_count % warn_count == 0 && iter_count >= warn_count)
        {
          ERRMSG (_ ("quite many symlink hops, hope we're not looping..."),
                  fname, dir);
        }

      while (1)
        {
          result = readlink (fname, buf, blen);
          if (result == -1 && errno != EINVAL)
            {
              PRERR(dir, fname);
              return 0;
            }
          if (result == blen)
            {
              CHAT (_ ("buffer was too small, resizing..."), fname);
              blen += blen_inc;
              buf = gc_xrealloc (buf, blen);
              continue;
            }
          if (result != -1)
            {
              CHAT    (_ ("read link"), fname);
              buf [result] = '\0';  /* Terminate the string. */
              VERBOSE ("-> ", buf);
              fname = buf; /* In the next round we'll use this file. */
            }
          break;
        }
      /* We want to be in the directory that contains the file fname;
	 however, we can only use its base name from now on. */
      xchdir (fname_dir (fname), 0);
      fname = fname_chopdir (fname);
    }
  while (result != -1);

  /* Now, we want the absolute path name to fname. */
  fname = fname_absolutify (fname);

  xchdir (wd, 0);

  /* When we reach this point, we have a genuine non-symlink in
     fname. */
  if (fname != buf)
    {
      /* Make the return value malloc()ed. */
      fname = gc_xstrdup (fname); 
    }
  return fname;
}

/* Chase name and report to user.  Return zero iff the name was
   nonexistent or a dangling symlink. */
static int
chase_and_report (const char * name)
{
  char * s;
  char * fname;
  
  fname = gc_xstrdup (name);

  VERBOSE ("", fname);
  s = chase_symlink (fname);
  if (s == 0)
    return 0;
  else
    {
      puts (s);
      return 1;
    }
}

/* Chase all file names in f.  Return zero iff some name was
   nonexistent or a dangling symlink.  fname is used in diagnostics.
   reader is used to get the file name from f.  It is supposed to be
   either freadline or fread_until_null. */
static int
handle_file(FILE * f, char * (*reader)(FILE *, const char *),
            const char * fname)
{
  char * line;  
  int rv = 1;

  VERBOSE (_ ("Reading file names from "), fname);
  
  line = reader (f, fname);
  while (line != 0)
    {
      if (line [0] != '\0')
        rv = chase_and_report (line) ? rv : 0;
      else
        CHAT (_ ("empty file name found"), fname);
      
      line = reader (f, fname);
    }
  
  assert (line == 0);
  if (feof (f))
    CHAT (_ ("bumped to eof"), fname);
  else
    PRERR("", fname);

  return rv;
}

/* main() was adopted from the example in GNU's getopt_long(3) manual
   page. */
int
main (int argc, char * argv[])
{
  int c;
  int rv = EXIT_SUCCESS;
  int from_file = 0;
  char * (*reader)(FILE *, const char *)  = freadline;

  /* i18n */
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  program_name = argv[0];

  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
      {
	{"from-file",              0, 0,        (int) 'f'},
        {"loop-warn-threshold",     1, 0,       (int) 'w'},
        {"loop-fail-threshold",     1, 0,       (int) 'l'},
        /* the next two are just for compatibility's sake */
        {"loop-warn-treshold",     1, 0,        (int) 'w'},
        {"loop-fail-treshold",     1, 0,        (int) 'l'},
        
        {"disable-loop-detection", 0, 0,        (int) 'D'},
        {"null",                   0, 0,        (int) '0'},
	{"verbose",                0, &verbose, 1        },
	{"help",                   0, 0,        (int) 'h'},
	{"version",                0, 0,        (int) 'v'},
	{0,                        0, 0,         0}
      };
      
      c = getopt_long (argc, argv, "0Dfhl:vw:",
		       long_options, &option_index);
      if (c == -1)
	break;

      switch (c)
	{
        case 'D':
          record_chain_p = 0;
          break;

        case '0':
          reader = fread_until_null;
          /* fall through to 'f' */

	case 'f':
	  from_file = 1;
	  break;

	case 'v':
	  printf ("chase %s\n", VERSION);
	  printf ("Copyright (C) %s Antti-Juhani Kaijanaho.\n",
		  COPYRIGHT_YEAR);
	  puts   (_ ("This program comes with NO WARRANTY,"));
	  puts   (_ ("to the extent permitted by law."));
	  puts   (_ ("You may redistribute copies of this program"));
	  puts   (_ ("under the terms of the GNU General Public License,"));
	  puts   (_ ("version 2 or later.  For more information about"));
	  printf (_ ("these matters, see the file %s .\n"), COPYING_LOCATION);
	  return 0;
	  

	case 'h':
	  printf ("%s: %s [options ... ] [file ... ]\n"
		  "       %s -h|--help\n       %s -v|--version\n",
		  _ ("Usage"), program_name, program_name, program_name);
	  puts   ("");
	  puts   (_ ("Options:"));
	  puts   ("   --verbose");
          puts   (_ ("     Chat about what is being done."));
          puts   ("   -D, --disable-loop-detection");
          puts   (_ ("     Do not keep a record of the chain of symlinks relevant"));
          puts   (_ ("     to the file name being chased.  This inhibits reliable"));
          puts   (_ ("     detection of symlink loops"));
	  puts   ("   -f, --from-file");
	  puts   (_ ("     Treat the file names as sources for names to chase."));
          puts   ("   -0, --null");
          puts   (_ ("     Treat null as the file name separator instead of newline."));
          puts   (_ ("     Implies -f."));
          puts   ("   -w WCOUNT, --loop-warn-threshold=WCOUNT");
          puts   ("   -l LCOUNT, --loop-fail-threshold=LCOUNT");
          puts   (_ ("     Set the threshold for warning about a possible symlink loop"));
          puts   (_ ("     (WCOUNT) and for failing because of one (LCOUNT).  Zero"));
          printf (_ ("     disables the check.  Default values: WCOUNT %lu, LCOUNT %lu.\n"),
                  warn_count, giveup_count);
          puts   ("   -h, --help");
          puts   (_ ("     Output this usage summary and exit successfully."));
	  puts	 ("   -v, --version");
	  puts   (_ ("     Show version information and exit successfully."));
	  puts   ("");
	  puts   (_ ("Please report bugs to <gaia@iki.fi>."));
	  return 0;

        case 'l':
          {
            char * tail;

            errno = 0;
            giveup_count = strtoul (optarg, &tail, 10);
            if (errno != 0)
              {
                perror ("loop-fail-threshold");
                exit (EXIT_FAILURE);
              }
          }            
          break;

        case 'w':
          {
            char * tail;

            errno = 0;
            warn_count = strtoul (optarg, &tail, 10);
            if (errno != 0)
              {
                perror ("loop-warn-threshold");
                exit (EXIT_FAILURE);
              }
          }            
          break;

	default:
	  break;
	}
    }
  
  if (from_file)
    {
      if (optind == argc)
        {
          rv = handle_file (stdin, reader, gc_xstrdup ("(stdin)"))
            ? rv : EXIT_FAILURE;
        }
      else for (/* using inherited optind */; optind < argc; optind++)
	{
	  FILE * f;
	  char * fname;

	  if (strcmp (argv [optind], "-") == 0)
	    {
	      f = stdin;
	      fname = gc_xstrdup ("(stdin)");
	    }
	  else
	    {
	      fname = argv [optind];
	      f = fopen (fname, "r");
	      if (f == NULL)
		{
		  PRERR ("", fname);
		  continue;
		}
	    }

          rv = handle_file (f, reader, fname) ? rv : EXIT_FAILURE;

          if (f != stdin)
            fclose (f);
	}
    }
  else
    {
      for (/* using inherited optind */; optind < argc; optind++)
	rv = chase_and_report (argv [optind]) ? rv : EXIT_FAILURE;
    }

  return rv;
}

